# Why?

This repository contains a Dockerfile used to create an image to build and deploy microservices using:
- Go
- Serverless

It's meant to be used as a base image for Gitlab CI.

This image can also be used to run the service locally.

# Where?

The images are hosted in: https://hub.docker.com/r/qventura/continuous-integration-golang-serverless

# How?

On push on master, the resulting image is tagged as `latest` and pushed on `hub.docker.com`

# TODO

Add more granular tagging based on git tag
