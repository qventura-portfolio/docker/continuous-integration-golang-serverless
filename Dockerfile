FROM golang:alpine

RUN apk add --update \
  git \
  nodejs \
  nodejs-npm

RUN npm install -g serverless

RUN apk add --update make
